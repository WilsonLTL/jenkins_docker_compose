# Jenkins_docker_compose
## Easy setup Jenkins docker-compose
![image1](https://i.imgur.com/bzPaiv2.png)
![image2](https://i.imgur.com/S16VAew.png)

### environment var
```
access_token_id : AKIAQC6HQWXHCFDFQKMQ
access_token_key : FXpoKwlbtyJ8VNbIVOX5/1N2//uxiRkM9abYFdI7
```
### Setting
```
1. Clone the project
2. Create a new folder names "jenkins_home"
3. docker-compose up
4. Enjoy
```

## Set up in relay ec2
```text
1. Set the environment (such as python, npm etc.)
2. sudo nano .netrc
3. in file, paste:
    machine gitlab.com
    login Username
    password Access_token

```

## Insert new ssh credentials
```
1. Click "credentials" -> "global" -> "Add Credentials".
2. In "Kind", select "SSH Username with provate key".
3. In "Private", check "Enter directly" and paste your key.
4. Click "save" to insert a new record.
PS: In "username", please enter your ec2/ebs username (i.e. ubuntu) for ssh host
```

## Insert new ssh host
```
1. Click "管理 Jenkins" -> "設定系統"
2. In "SSH remote hosts", insert "hostname", "Port" and select the "credentials"
PS1: "hostname" i.e. "ec2-3-0-59-72.ap-southeast-1.compute.amazonaws.com"
PS2: "port" i.e. "22"
```

## ec2
### Deploy in ec2 (Docker)
```
sudo docker login registry.gitlab.com -u {useremail} -p {access_token}
sudo docker stop $(sudo docker ps -aq)
sudo docker pull registry.gitlab.com/{username}/{project_name}
sudo docker run -d -p 80:5000 registry.gitlab.com/{username}/{project_name}
```

### Deploy in ec2 (Code)
```
sudo rm -rf ~/{project_name}
git clone https://gitlab.com/WilsonLTL/live2d_model_websdk.git
Run command ...
```

## ebs
### Deploy in ebs
```
#!/bin/bash -ilex
sudo rm -rf ~/.aws/
sudo rm -rf ~/{project_name}
mkdir ~/.aws/
touch ~/.aws/credentials
printf "[eb-cli]\naws_access_key_id = %s\naws_secret_access_key = %s\n" "{access_token_id}" "{access_token_key}" >> ~/.aws/credentials
touch ~/.aws/config
printf "[profile eb-cli]\nregion=us-west-2\noutput=json" >> ~/.aws/config
git clone https://gitlab.com/WilsonLTL/live2d_model_websdk.git
cd "/home/ubuntu/{project_name}/"
eb init {ebs_application_name} -r ap-southeast-1 -p docker
eb deploy {ebs_environment_name}
sudo rm -rf ~/{project_name}
PS: modify docker to  other code environment as you want
```

## s3
### Deploy in s3
```
sudo rm -rf ~/.aws/
sudo rm -rf ~/{project_name}
mkdir ~/.aws/
touch ~/.aws/credentials
printf "[default]\naws_access_key_id = %s\naws_secret_access_key = %s\n" "{access_token_id}" "{access_token_key}" >> ~/.aws/credentials
git clone https://gitlab.com/WilsonLTL/live2d_model_websdk.git
cd "/home/ubuntu/{project_name}/"
aws s3 cp "." s3://{bucket_name}/ --recursive
sudo rm -rf ~/{project_name}
```
